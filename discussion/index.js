function mod(){
    // return 9+2
    // return 9-2
    // return 9*2
    // return 9/2
    let x = 10
    // return x += 2
    // return x -= 2
    // return x *= 2
    // return x /= 2
    return x %=2
};

console.log(mod());

// Assignment Operator
let x = 1;
let sum = 1;
sum = sum + 1;
sum += 1;

console.log(sum);
console.log(x);

let z = 1;
// let z = (1) + 4;
let increment = ++z;

// Pre-increment
console.log(`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);


// Post-increment
increment = z++;
console.log(`Result of post-increment: ${increment}`);
console.log(`Result of post-increment: ${z}`);

// Pre-decrement
let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);

// Post-decrement
decrement = z--;
console.log(`Result of post-decrement: ${decrement}`);
console.log(`Result of post-decrement: ${z}`);

// Comparison Operators
// Equality Operator (==)
let juan = "juan";

console.log(1 == 1);
console.log(0 == false);
console.log(juan =="juan");

// Strict Equality (===)
console.log(1 === true);
console.log(juan === "Juan");

// inequality (!=)
console.log(1 != 1);
console.log(juan !="juan");

// Strict inequality (!==)
console.log(1 !== 1);

// Logical Operators

// And Operator
let isLegalAge = false;
let isRegistered = false;

let allRequirementMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementMet}`);

// Or Operator (||)
allRequirementMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${allRequirementMet}`);

// Selection Control Structures

// IF Statements

let num = -1;
if(num<0){
    console.log("Hello")
};

let value =30;
if(value > 10 && value < 40){
    console.log("Welcome to Zuitt")
};

// if-else statement

num = 5
if (num>10){
    console.log(`Number is greater then 10`)
}
else{
    console.log(`Number is less than 10`)
};

num = parseInt(prompt("Please input a number."));

if(num>59){
    alert("Senior Age")
    console.log(num)
}
else{
    alert("Invalid Age")
    console.log(num)
};

// if-else if-else statement

// 1. Quezon
// 2. Valenzuela
// 3. Pasig
// 4. Taguig

let city = parseInt(prompt("Enter a Number"));
if(city === 1){
    alert("Welcome to Quezon City")
}
else if (city === 2){
    alert("Welcome to Valenzuela City")
}
else if (city === 3){
    alert("Welcome to Pasig City")
}
else if (city === 4){
    alert("Welcome to Taguig City")
}
else{
    alert("Invalid Number")
}

let message = ""

function determineTyphoonIntensity(windspeed){
    if(windspeed < 30){
        return `Not a typhoon yet.`
    }
    else if(windspeed <= 61){
        return `Tropical Depression detected.`
    }
    else if(windspeed >= 62 && windspeed <= 88){
        return`Tropical Storm detected`
    }
    else if(windspeed >= 89 && windspeed <= 117){
        `Severe Tropical Storm detected`
    }
    else{
        return `Typhoon detected`
    }
}

message = determineTyphoonIntensity(70);
console.log(message);

// Ternary operator

let ternaryResult = (1 < 18) ? "1" : "0";
console.log(`Result of ternary operator: ${ternaryResult}`);

let name;
function isOfLegalAge(){
    name = "John";
    return "You are of the age limit"
};

function isUnderAge(){
    name = "Jane";
    return "You are uynder the legal Age"
};
let age = parseInt(prompt("What is your age?"));
let LegalAge = (age >= 18) ?isOfLegalAge() : isUnderAge();
alert(`Result of Ternary operator in Function: ${LegalAge} ${name}`)

// Switch Statement

let day = prompt("What day is it today?").toLowerCase();
switch (day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		a
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");
};

// Try-Catch-Finally Statement

function showIntensityAlert(windspeed){
    try{
        alert(determineTyphoonIntensity(windspeed));
    }
    catch(error){
        console.log(typeof error);
        console.warn(error.message);
    }
    finally{
        alert("Intensity updates will show new alert")
    }
}

showIntensityAlert(56);